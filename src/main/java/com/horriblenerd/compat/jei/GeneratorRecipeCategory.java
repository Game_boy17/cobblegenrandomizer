package com.horriblenerd.compat.jei;

import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

/**
 * Created by HorribleNerd on 12/11/2020
 */
public class GeneratorRecipeCategory extends GeneratorRecipeCategoryBase<GeneratorRecipeWrapper> {

    public final ResourceLocation UID;

    public GeneratorRecipeCategory(IGuiHelper guiHelper, Item icon, String name, ResourceLocation id, int size) {
        super(guiHelper, new ItemStack(icon), name, size);
        this.UID = id;
    }

    public GeneratorRecipeCategory(IGuiHelper guiHelper, ItemStack icon, String name, ResourceLocation id, int size) {
        super(guiHelper, icon, name, size);
        this.UID = id;
    }

   
    @Override
    public RecipeType<GeneratorRecipeWrapper> getRecipeType() {
        return new RecipeType<>(UID,GeneratorRecipeWrapper.class);
    }
}
